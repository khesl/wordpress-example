#FROM wordpress:php8.0-fpm-alpine
FROM wyveo/nginx-php-fpm:php80

ARG ROOT_PATH="/usr/share/nginx/html"
ARG WEB_USER="nginx"
ARG WEB_GROUP="nginx"

COPY . ${ROOT_PATH}
COPY ./.config/nginx/conf.d/* /etc/nginx/conf.d

#RUN chown -R ${WEB_USER}:${WEB_GROUP} ${ROOT_PATH}/*

WORKDIR ${ROOT_PATH}
#RUN composer install


# Expose ports
EXPOSE 80

#CMD ["/start.sh"]
