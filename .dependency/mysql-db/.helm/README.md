# Настройка MySql подов в кубе, на персистенс основе.

## Источники

- [оф дока](https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/)

## Pre install
Need create all folders for working persistence DB


## Deploy

### Helm-deploy

- протестировать создаваемые ресурсы:
	```bash
	helm install --dry-run --debug --name **chart-name** --namespace **namespace-name** **chart-path**
	```

- деплой чарта:
	```bash
	helm install --name **chart-name** --namespace **namespace-name** **chart-path**
	```
	