# Mysql Dependency

### Helm-deploy

- протестировать создаваемые ресурсы:
	```bash
	helm install --dry-run --debug --name **chart-name** --namespace **namespace-name** **chart-path**
	```
- деплой чарта:
	```bash
	helm install --name **chart-name** --namespace **namespace-name** **chart-path**
	```

- обновление чарта:
	```bash
	helm upgrade **chart-name** --namespace **namespace-name** .helm
	```
	
- удаление чарта: 
	```bash
	helm delete --purge **chart-name**